var config = require("config.js");

//统一的网络请求方法
function request(params, isGetTonken) {
  // 全局变量
  var globalData = getApp().globalData;
  // 如果正在进行登陆，就将非登陆请求放在队列中等待登陆完毕后进行调用
  if (!isGetTonken && globalData.isLanding) {
    globalData.requestQueue.push(params);
    return;
  }
  const token = wx.getStorageSync('token');
  
  wx.request({
    url: config.domain + params.url, //接口请求地址
    data: params.data,
    header: {
      'Authorization': params.login || params.url.indexOf('/api/') > -1 ? undefined : token
    },
    method: params.method == undefined ? "POST" : params.method,
    dataType: 'json',
    responseType: params.responseType == undefined ? 'text' : params.responseType,
    success: function(res) {
      if (res.statusCode == 200) {
        //如果有定义了params.callBack，则调用 params.callBack(res.data)
        if (params.callBack) {
          if(res.data.code != 200){
            setTimeout( () => {
              wx.showToast({
                title: res.data.msg,
                icon: "none",
              });
            },0);
          }else {
            params.callBack(res.data);
          }
        }
      }else if (res.statusCode == 500) {
        setTimeout( () => {
          wx.showToast({
            title: "服务器出了点小差",
            icon: "none",
          });
        },0);
      } else if (res.statusCode == 401) {
        // 添加到请求队列
        globalData.requestQueue.push(params);
        // 是否正在登陆
        if (!globalData.isLanding) {
          globalData.isLanding = true
          //重新获取token,再次请求接口
          getToken();
        }
      } else if (res.statusCode == 400) {
        setTimeout( () => {
          wx.showToast({
            title: res.data,
            icon: "none",
          });
        },0);
      } else {
        //如果有定义了params.errCallBack，则调用 params.errCallBack(res.data)
        if (params.errCallBack) {
          params.errCallBack(res);
        }
      }
      if (!globalData.isLanding) {
        wx.hideLoading();
      }
    },
    fail: function(err) {
      wx.hideLoading();
      setTimeout( () => {
        wx.showToast({
          title: "服务器出了点小差",
          icon: "none",
        });
      },0);
    }
  })
}

//通过code获取token,并保存到缓存
var getToken = function(callBack) {
  wx.login({
    success: res => {
      var globalData = getApp().globalData;
      globalData.isLanding = true;
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      request({
        login: true,
        url: '/userApi/login',
        data: {
          appId: globalData.appId,
          jsCode: res.code
        },
        callBack: result => {
          if (result.data.status != 0) {
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '您已被禁用，不能购买，请联系客服'
            })
            wx.setStorageSync('token', '');
          } else {
            wx.setStorageSync('token', 'Bearer ' + result.data.token); //把token存入缓存，请求接口数据时要用
          }
          wx.setStorageSync('userId', result.data.id);
          wx.setStorageSync('openId', result.data.wxOpenid);
          globalData.isLanding = false;
          while (globalData.requestQueue.length) {
            request(globalData.requestQueue.pop());
          }
          if(callBack){
            callBack(result)
          }
        }
      }, true)

    }
  })
}

exports.request = request;
exports.getToken = getToken;