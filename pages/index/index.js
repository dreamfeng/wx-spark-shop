const app = getApp()
var http = require('../../utils/http.js');

Page({
  data: {
    active: 0,
    swipers: [],
    vertical: false,
    interval: 2000,
    duration: 500,
    time: 30 * 60 * 60 * 1000,
    timeData: {},
    sanimationData: {},
    coupons: [],
    killList: [],
    keywords: [ '精选','年货','新品','推荐'],
    goodsList: []
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.initData()
  },
  //事件处理函数
  bindViewTap: function () {

  },
  onReady: function () {
    // 页面加载获取数据
    this.initData()
  },
  onShow: function () {
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    var next = true;
    //连续动画关键步骤
    setInterval(function () {
      if (next) {
        this.animation.scale(0.95).step()
        next = !next;
      } else {
        this.animation.scale(1).step()
        next = !next;
      }
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 500)
  },
  initData() {
    this.getSwiperData()
    this.getSkillData()
    this.getGoodsData(1,'精选')
    this.getCouponData()
  },
  getSwiperData: function() {
    //加载Swiper列表
    var ths = this;
    var params = {
      url: "/api/swiper",
      method: "GET",
      callBack: function (res) {
        ths.setData({
          swipers: res.data
        });
      }
    };
    http.request(params);
  },
  getSkillData: function() {
    //加载秒杀产品列表
    var ths = this;
    var params = {
      url: "/api/seckill/goods",
      data: {
        secKillId: 1,
        current: 1,
        size: 6
      },
      method: "GET",
      callBack: function (res) {
        res.data.records.forEach(element => {
          element = ths.formatePrice(element,'killPrice')
        });
        ths.setData({
          killList: res.data.records
        });
        wx.stopPullDownRefresh()
      }
    };
    http.request(params);
  },
  getGoodsData: function(current,keywords){
    var ths = this;
    wx.showLoading({ title: '加载中'});
    var params = {
      url: "/api/goods/page",
      data: {
        keywords: keywords,
        current: current,
        size: 20
      },
      method: "GET",
      callBack: function (res) {
        res.data.records.forEach(element => {
          element = ths.formatePrice(element,'price')
        });
        ths.setData({
          goodsList: res.data.records
        });
        wx.stopPullDownRefresh()
        wx.hideLoading();
      }
    };
    http.request(params);
  },
  formatePrice: function(element,attribute){
    // 格式化金额
    var priceStr = '' + element[attribute];
    element['integerStr'] = priceStr.split('.')[0];
    element['decimalStr'] = priceStr.split('.')[1] ? '.' + priceStr.split('.')[1] : '.0';
    return element;
  },
  /**
   * 获取优惠券
   */
  getCouponData(){
    var ths = this;
    var params = {
      url: "/api/coupon",
      data: {
        limit: 3
      },
      method: "GET",
      callBack: function (res) {
        ths.setData({
          coupons: res.data
        });
      }
    };
    http.request(params);
  },
  /**
   * 页面跳转
  */
  goPages: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
  onTimeChange: function(e) {
    // 倒计时
    this.setData({
      timeData: e.detail,
    });
  },
  tabsChange: function(event){
    // tab页切换重新加载商品数据
    this.getGoodsData(1,event.detail.title)
  }
})
