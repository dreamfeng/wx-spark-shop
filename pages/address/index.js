var http = require('../../utils/http.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  getData(){
    var ths = this;
    wx.showLoading();
    const userId = wx.getStorageSync('userId')
    //加载用户地址信息
    var params = {
      url: "/userApi/address",
      method: "GET",
      data: {
        userId: userId
      },
      callBack: function(res) {
        ths.setData({
          items: res.data
        });
        wx.hideLoading();
      }
    };
    http.request(params);
  },
  onEdit: function(event){
    wx.navigateTo({
      url: "/pages/address/edit/index?id="+event.currentTarget.dataset.value
    });
  },
  onNewEdit: function(){
    wx.navigateTo({
      url: "/pages/address/edit/index"
    });
  }
})