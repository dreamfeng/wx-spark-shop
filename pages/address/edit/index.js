var http = require('../../../utils/http.js');
var areaList = require('../../../libs/data/area.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    areaList: areaList.default,
    autosize: {maxHeight: 100, minHeight: 50},
    formData:{
      id: null,
      name: null,
      province: null,
      provinceId: null,
      city: null,
      cityId: null,
      area: null,
      areaId: null,
      address: null,
      mobile: null,
      areaStr: null,
      userId: wx.getStorageSync('userId'),
      isDefault: false
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id){
      this.getData(options.id)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  getData: function(id){
    var ths = this;
    wx.showLoading();
    //加载用户地址信息
    var params = {
      url: "/userApi/address/"+id,
      method: "GET",
      callBack: function(res) {
        res.data['areaStr'] = res.data.province +'/'+ res.data.city+'/'+res.data.area
        ths.setData({
          formData: res.data
        });
        wx.hideLoading();
      }
    };
    http.request(params);
  },
  onClick: function(e){
    this.setData({
      show: true
    });
  },
  onCancel:function(){
    this.setData({
      show: false
    });
  },
  onConfirm: function(e){
    const formData = this.data.formData
    formData.provinceId = e.detail.values[0].code
    formData.province = e.detail.values[0].name
    formData.cityId = e.detail.values[1].code
    formData.city = e.detail.values[1].name
    formData.areaId = e.detail.values[2].code
    formData.area = e.detail.values[2].name
    formData.areaStr = formData.province +'/'+ formData.city+'/'+formData.area
    this.setData({
      formData: formData,
      show: false
    });
  },
  onSwitchChange({ detail }){
    // switch 默认选中事件
    this.data.formData.isDefault = detail
    this.setData({ formData: this.data.formData });
  },
  onInputEdit(e){
    // 数据的双向绑定
    let value = e.detail.value;
    let obj = e.currentTarget.dataset.obj;
    this.data.formData[obj] = value
    this.setData({ formData: this.data.formData });
  },
  onSubmit: function(){
    if(this.data.formData.name == null){
      wx.showToast({ title: "请输入收货人!", icon: "none"});
      return
    }
    if(this.data.formData.mobile == null){
      wx.showToast({ title: "请输入手机号!", icon: "none"});
      return
    }
    if(this.data.formData.provinceId == null){
      wx.showToast({ title: "请输入地区!", icon: "none"});
      return
    }
    if(this.data.formData.address == null){
      wx.showToast({ title: "请输入详细地址!", icon: "none"});
      return
    }
    //提交用户地址信息
    var params = {
      url: "/userApi/address",
      method: "POST",
      data: this.data.formData,
      callBack: function(res) {
        wx.hideLoading();
        wx.navigateTo({
          url: '/pages/address/index',
        })
      }
    };
    http.request(params);
  },
  onDelete: function(){
    // 删除地址
    let url = "/userApi/"+this.data.formData.userId+"/address/"+this.data.formData.id
    var params = {
      url: url,
      method: "DELETE",
      callBack: function(res) {
        wx.hideLoading();
        wx.navigateTo({
          url: '/pages/address/index',
        })
      }
    };
    http.request(params);
  }
})