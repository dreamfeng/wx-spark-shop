var http = require('../../utils/http.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [],
    current: 1,
    categoryIds: '',
    option1: [
      { text: '全部商品', value: 0 },
      { text: '新款商品', value: 1 },
      { text: '活动商品', value: 2 },
    ],
    option2: [
      { text: '默认排序', value: 'create_date,true' },
      { text: '销量降序', value: 'sale_num,true' },
      { text: '销量升序', value: 'sale_num,false' },
      { text: '价格降序', value: 'min_price,true' },
      { text: '价格升序', value: 'min_price,false' },
    ],
    value1: 0,
    value2: 'create_date,true',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      categoryIds: options.categoryIds
    })
    this.getGoodsData()
  },
  getGoodsData: function () {
    var ths = this;
    wx.showLoading({ title: '加载中' });
    const data = {
      categoryIds: this.data.categoryIds,
      current: this.data.current,
      size: 20,
      orderBy: this.data.value2.split(',')[0],
      asc: this.data.value2.split(',')[1],
    }
    if(this.data.value1 != 0){
      data['isHot'] = true
    }
    var params = {
      url: "/api/goods/page",
      data: data,
      method: "GET",
      callBack: function (res) {
        res.data.records.forEach(element => {
          element = ths.formatePrice(element)
        });
        ths.setData({
          goodsList: res.data.records
        });
        wx.stopPullDownRefresh()
        wx.hideLoading();
      }
    };
    http.request(params);
  },
  formatePrice: function (element) {
    // 格式化金额
    var priceStr = '' + element.price;
    element['integerStr'] = priceStr.split('.')[0];
    element['decimalStr'] = priceStr.split('.')[1] ? '.' + priceStr.split('.')[1] : '.0';
    return element;
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  goPages: function (e) {
    //页面跳转
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
})