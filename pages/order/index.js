const app = getApp()
var http = require('../../utils/http.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    current: 1,
    isAll: false,
    activeStatus: null,
    tabs: [
      {
        title: '全部',
        value: null,
      },
      {
        title: '待付款',
        value: 0,
      },
      {
        title: '待发货',
        value: 2,
      },
      {
        title: '待收货',
        value: 3,
      },
      {
        title: '售后',
        value: 5,
      }
    ],
    orders: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initData(null)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 拉取订单数据
   */
  initData(){
    var ths = this;
    wx.showLoading({ title: '加载中'});
    const data = {
      current: this.data.current,
      size: 10,
      userId: wx.getStorageSync('userId'),
    }
    if(this.data.activeStatus != null){
      data['orderStatus'] = this.data.activeStatus
    }
    var params = {
      url: "/orderApi/page",
      method: "GET",
      data: data,
      callBack: function (res) {
        let orders = res.data.records
        let isAll = this.data.isAll
        if(res.data.current == res.data.pages){
          isAll = true
        }
        if(ths.data.orders.length > 0){
          orders = ths.data.orders.concat(orders)
        }
        ths.setData({
          orders,
          isAll
        });
        wx.hideLoading();
      }
    };
    http.request(params);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.isAll) {
      return
    }
    this.data.current = this.data.current+1
    this.initData()
  },
  onTabsChange: function(e){
    let tab = this.data.tabs[e.detail.index];
    this.setData({
      activeStatus: tab.value
    });
    // 切换tab页签
    this.initData()
  }
})