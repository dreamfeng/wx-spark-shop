var http = require('../../utils/http.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeId: 1,
    mainActiveIndex: 0,
    categorys: [],
    categoryNav:[],
    categoryItem: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.loadData()
  },
  loadData(){
    var ths = this;
    const { categoryNav,categorys } = this.data;
    let categoryItem = []
    wx.showLoading();
    //加载订单列表
    var params = {
      url: "/api/goods/category",
      method: "GET",
      data: {
        level: 3
      },
      callBack: function(res) {
        res.data.forEach((e,i) => {
          categoryNav.push( { id: e.id, text: e.text} )
          categorys.push(e)
          if(i == 0){
            categoryItem = e.children
          }
        })
        ths.setData({
          categoryNav,
          categorys,
          categoryItem: categoryItem
        });
        wx.hideLoading();
      }
    };
    http.request(params);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  onClickNav({ detail = {} }) {
    this.setData({
      mainActiveIndex: detail.index || 0,
      categoryItem: this.data.categorys[detail.index].children
    });
  },
  /**
  * 页面跳转
  */
  goPages: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
  gridItemClick: function(event){
    const category = event.currentTarget.dataset.value;
    const categoryIds = category.pids.replace('0,','')+','+category.id
    wx.navigateTo({
      url: '/pages/goodsList/index?categoryIds='+categoryIds
    });
  }
})