var http = require('../../utils/http.js');

Page({
  /**
   * 页面的初始数据
   */
  data: {
    value: '',
    historySearchs: [],
    goodsList: [],
    current: 1,
    isAll: false,
    hiddenHistory: false,
    option1: [
      { text: '全部商品', value: 0 },
      { text: '新款商品', value: 1 },
      { text: '活动商品', value: 2 },
    ],
    option2: [
      { text: '默认排序', value: 'create_date,true' },
      { text: '销量降序', value: 'sale_num,true' },
      { text: '销量升序', value: 'sale_num,false' },
      { text: '价格降序', value: 'min_price,true' },
      { text: '价格升序', value: 'min_price,false' },
    ],
    value1: 0,
    value2: 'create_date,true',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      historySearchs: wx.getStorageSync('historySearchs') || []
    });
  },

    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.isAll) {
      return
    }
    this.data.current = this.data.current+1
    this.getGoodsData(true)
  },
  onSearch(event) {
    // 确定搜索
    var historySearchs = this.data.historySearchs;
    if(event.detail != ''){
      var pos = historySearchs.indexOf(event.detail);
      if(pos > -1){
        historySearchs.splice(pos, 1);
      }
      historySearchs.unshift(event.detail)
      // 存储 搜索历史
      wx.setStorageSync('historySearchs', historySearchs)
    }
    this.setData({
      value: event.detail,
      historySearchs,
      hiddenHistory: true
    });
    this.getGoodsData(false)
  },
  onFocus: function() {
    // 获取焦点时
    this.setData({
      hiddenHistory: false
    });
  },
  onDelete: function(){
    var ths = this;
    wx.showModal({
      content: '是否确认删除历史记录',
      success (res) {
        if (res.confirm) {
          wx.removeStorageSync('historySearchs')
          ths.setData({
            historySearchs: []
          });
        }
      }
    })    
  },
  onDropdownChange1: function(event){
    this.setData({
      value1: event.detail,
      current: 1,
      isAll:  false
    });
    this.getGoodsData(false);
  },
  onDropdownChange2: function(event){
    this.setData({
      value2: event.detail,
      current: 1,
      isAll:  false
    });
    this.getGoodsData(false);
  },
  getGoodsData: function (isPullDown) {
    var ths = this;
    wx.showLoading({ title: '加载中' });
    const data = {
      title: this.data.value,
      current: this.data.current,
      size: 10,
      orderBy: this.data.value2.split(',')[0],
      asc: this.data.value2.split(',')[1],
    }
    if(this.data.value1 != 0){
      data['isHot'] = true
    }
    var params = {
      url: "/api/goods/page",
      data: data,
      method: "GET",
      callBack: function (res) {
        let goodsList = res.data.records
        goodsList.forEach(element => {
          element = ths.formatePrice(element)
        });
        let isAll = this.data.isAll
        if(res.data.current == res.data.pages){
          isAll = true
        }
        if(ths.data.goodsList.length > 0 && isPullDown){
          goodsList = ths.data.goodsList.concat(goodsList)
        }
        ths.setData({
          goodsList,
          isAll
        });
        wx.hideLoading();
      }
    };
    http.request(params);
  },
  formatePrice: function (element) {
    // 格式化金额
    var priceStr = '' + element.price;
    element['integerStr'] = priceStr.split('.')[0];
    element['decimalStr'] = priceStr.split('.')[1] ? '.' + priceStr.split('.')[1] : '.0';
    return element;
  },
  goPages: function (e) {
    //页面跳转
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
  onHistoryItem: function(event){
    const value = event.currentTarget.dataset.value
    this.setData({
      value,
      hiddenHistory: true
    });
    this.getGoodsData(false)
  }
})