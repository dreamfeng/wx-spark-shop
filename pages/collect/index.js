var http = require('../../utils/http.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 1,
    isAll: false,
    goodsList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 加载数据
   */
  initData: function (){
    var ths = this;
    wx.showLoading({ title: '加载中'});
    var params = {
      url: "/userApi/collect",
      method: "GET",
      data: {
        current: this.data.current,
        size: 10,
        userId: wx.getStorageSync('userId')
      },
      callBack: function (res) {
        let goodsList = res.data.records
        goodsList.forEach(element => {
          element = ths.formatePrice(element)
        });
        let isAll = this.data.isAll
        if(res.data.current == res.data.pages){
          isAll = true
        }
        if(ths.data.goodsList.length > 0){
          goodsList = ths.data.goodsList.concat(goodsList)
        }
        ths.setData({
          goodsList,
          isAll
        });
        wx.hideLoading();
      }
    };
    http.request(params);
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.isAll) {
      return
    }
    this.data.current = this.data.current+1
    this.initData()
  },
  formatePrice: function (element) {
    // 格式化金额
    var priceStr = '' + element.price;
    element['integerStr'] = priceStr.split('.')[0];
    element['decimalStr'] = priceStr.split('.')[1] ? '.' + priceStr.split('.')[1] : '.0';
    return element;
  },
})