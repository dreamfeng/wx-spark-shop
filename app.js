var http = require('utils/http.js');

App({
  onLaunch: function () {
    http.getToken();
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              var ths = this;
              var params = {
                url: "/userApi",
                method: "POST",
                data:{
                  id: wx.getStorageSync('userId'),
                  nickname: ths.globalData.userInfo.nickname,
                  avatar: ths.globalData.userInfo.avatarUrl
                },
                callBack: function (res) {
                  ths.globalData.userInfo['userTypeName'] = res.data.userTypeName
                  ths.globalData.userInfo['createDate'] = res.data.createDate
                }
              };
              http.request(params);
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    appId: 'wx28480e43e3e8636f',
        // 定义全局请求队列
    requestQueue: [],
    // 是否正在进行登陆
    isLanding: false,
    userInfo: null
  }
})