# wx-spark-shop

#### 介绍
SPARK微信小程序

#### 软件架构
使用了vant-webapp作为主体UI设计,具体的vant-webapp 使用方法请参考[vant-webapp ](http://https://github.com/youzan/vant-weapp)


#### 预览


|   |   |   |
|---|---|---|
| ![主页](https://images.gitee.com/uploads/images/2021/0223/100752_7cc4cc24_1890906.jpeg "Screenshot_20210223_100243_com.tencent.mm.jpg")  | ![分类](https://images.gitee.com/uploads/images/2021/0223/101006_823282f6_1890906.jpeg "Screenshot_20210223_100322_com.tencent.mm.jpg")  |  ![购物车](https://images.gitee.com/uploads/images/2021/0223/101016_a4108ffe_1890906.jpeg "Screenshot_20210223_100326_com.tencent.mm.jpg") |
|  ![我的](https://images.gitee.com/uploads/images/2021/0223/101025_b66b1272_1890906.jpeg "Screenshot_20210223_100330_com.tencent.mm.jpg") | ![优惠券](https://images.gitee.com/uploads/images/2021/0223/101039_06d7022d_1890906.jpeg "Screenshot_20210223_100251_com.tencent.mm.jpg")  | ![商品详情](https://images.gitee.com/uploads/images/2021/0223/101107_93180bac_1890906.jpeg "Screenshot_20210223_100310_com.tencent.mm.jpg")  |
|  ![秒杀](https://images.gitee.com/uploads/images/2021/0223/101119_a0b5a0dd_1890906.jpeg "Screenshot_20210223_100257_com.tencent.mm.jpg") |  ![选择规格](https://images.gitee.com/uploads/images/2021/0223/101131_5844a9ba_1890906.jpeg "Screenshot_20210223_100314_com.tencent.mm.jpg") | ![下单](https://images.gitee.com/uploads/images/2021/0223/101149_f841f419_1890906.jpeg "Screenshot_20210223_100338_com.tencent.mm.jpg")  |
